// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const { exists } = require('fs');
const { stringify } = require('querystring');
const vscode = require('vscode');

// +--------------------------------------------------------------------------------------+
// |                                   Helper Functions                                   |
// +--------------------------------------------------------------------------------------+
let COMMENT_LINE_SYNTAX_DICT =
{
	"default": "//",
	"c": "//",
	"cpp": "//",
	"csharp": "//",
	"java": "//",
	"javascript": "//",
	"javascriptreact": "//",
	"php": "//",
	"json": "//",
	"jsonc": "//",
	"python": "#",
	"lua": "--",
	"bat": "REM ",
	"html": null,
	"xml": null,
	"xaml": null,
	"css": null,
};
let COMMENT_BLOCK_START_SYNTAX_DICT =
{
	"default": "/*",
	"c": "/*",
	"cpp": "/*",
	"csharp": "/*",
	"java": "/*",
	"javascript": "/*",
	"javascriptreact": "/*",
	"php": "/*",
	"json": "/*",
	"jsonc": "/*",
	"python": null,
	"lua": "--[[",
	"bat": null,
	"html": "<!--",
	"xml": "<!--",
	"xaml": "<!--",
	"css": "/*",
};
let COMMENT_BLOCK_END_SYNTAX_DICT =
{
	"default": "*/",
	"c": "*/",
	"cpp": "*/",
	"csharp": "*/",
	"java": "*/",
	"javascript": "*/",
	"javascriptreact": "*/",
	"php": "*/",
	"json": "*/",
	"jsonc": "*/",
	"python": null,
	"lua": "]]--",
	"bat": null,
	"html": "-->",
	"xml": "-->",
	"xaml": "-->",
	"css": "*/",
};
function getCommentLineSyntax(languageId)
{
	if (languageId in COMMENT_LINE_SYNTAX_DICT)
	{
		return COMMENT_LINE_SYNTAX_DICT[languageId];
	}
	else
	{
		return COMMENT_LINE_SYNTAX_DICT["default"];
	}
}
function getCommentBlockStartSyntax(languageId)
{
	if (languageId in COMMENT_BLOCK_START_SYNTAX_DICT)
	{
		return COMMENT_BLOCK_START_SYNTAX_DICT[languageId];
	}
	else
	{
		return COMMENT_BLOCK_START_SYNTAX_DICT["default"];
	}
}
function getCommentBlockEndSyntax(languageId)
{
	if (languageId in COMMENT_BLOCK_END_SYNTAX_DICT)
	{
		return COMMENT_BLOCK_END_SYNTAX_DICT[languageId];
	}
	else
	{
		return COMMENT_BLOCK_END_SYNTAX_DICT["default"];
	}
}

//TODO: Do we need to explicitely handle stuff like 0x## or 0b##?
function my_parseNumber(numStr, numType)
{
	if (numType == "bin")
	{
		return parseInt(numStr, 2);
	}
	else if (numType == "dec")
	{
		return parseInt(numStr, 10);
	}
	else if (numType == "hex")
	{
		return parseInt(numStr, 16);
	}
	else if (numType == "ascii")
	{
		return numStr.codePointAt(0);
	}
	else { return null; }
}
function my_encodeNumber(num, numType)
{
	if (numType == "bin")
	{
		var result = num.toString(2);
		// Pad to whole byte's worth
		let numZerosToAdd = (8 - (result.length % 8));
		if (numZerosToAdd < 8) { result = "0".repeat(numZerosToAdd) + result; }
		return result;
	}
	else if (numType == "dec")
	{
		return num.toString(10);
	}
	else if (numType == "hex")
	{
		var result = num.toString(16).toUpperCase();
		// Pad to whole byte's worth
		let numZerosToAdd = (2 - (result.length % 2));
		if (numZerosToAdd < 2) { result = "0".repeat(numZerosToAdd) + result; }
		return result;
	}
	else if (numType == "ascii")
	{
		return String.fromCharCode(num);
	}
	else { return null; }
}

function isWhitespace(testStr, allowNewLines = false)
{
	console.assert(typeof(testStr) == "string");
	for (var cIndex = 0; cIndex < testStr.length; cIndex++)
	{
		if (testStr[cIndex] != ' ' && testStr[cIndex] != '\t' &&
			(!allowNewLines || (testStr[cIndex] != '\n' && testStr[cIndex] != '\r')))
		{
			return false;
		}
	}
	return true;
}
function isHeaderOutlineCharacter(testChar)
{
	if (testChar == '+') { return true; }
	if (testChar == '-') { return true; }
	if (testChar == '=') { return true; }
	if (testChar == '|') { return true; }
	if (testChar == '#') { return true; }
	return false;
}

function getSelectedText()
{
	const documentText = vscode.window.activeTextEditor.document.getText();
	if (!documentText) { return ""; }
	const activeSelection = vscode.window.activeTextEditor.selection;
	if (activeSelection.isEmpty) { return ""; }
	const selStartoffset = vscode.window.activeTextEditor.document.offsetAt(activeSelection.start);
	const selEndOffset = vscode.window.activeTextEditor.document.offsetAt(activeSelection.end);
	let selectedText = documentText.slice(selStartoffset, selEndOffset).trim();
	return selectedText.replace(/\s\s+/g, " "); //TODO: Do we need this replace??
}

function getFormatGeneratedNum(format, numIndex)
{
	var startStr = format;
	var incrementStr = "1";
	if (format.includes(":"))
	{
		let formatParts = format.split(":");
		if (formatParts.length != 2) { return null; }
		startStr = formatParts[0];
		incrementStr = formatParts[1];
	}
	
	var generatingChars = false;
	var startNum = my_parseNumber(startStr, "dec");
	if (startNum === null || isNaN(startNum)) { startNum = my_parseNumber(startStr, "ascii"); generatingChars = true; }
	if (startNum === null || isNaN(startNum)) { return null; }
	
	var incrementNum = my_parseNumber(incrementStr, "dec");
	if (incrementNum === null || isNaN(startNum)) { incrementNum = my_parseNumber(startStr, "ascii"); }
	if (incrementNum === null || isNaN(startNum)) { return null; }
	
	let resultNum = startNum + incrementNum * numIndex;
	if (generatingChars)
	{
		return my_encodeNumber(resultNum, "ascii");
	}
	else
	{
		return my_encodeNumber(resultNum, "dec");
	}
}

function getColumnNumberForIndex(tabSize, lineStr, charIndex)
{
	var currColumn = 0;
	for (var cIndex = 0; cIndex < lineStr.length && cIndex < charIndex; cIndex++)
	{
		if (lineStr[cIndex] == '\t')
		{
			let actualTabSize = tabSize - (cIndex % tabSize);
			currColumn += actualTabSize;
		}
		else { currColumn++; }
	}
	return currColumn;
}

function doesLineLookLikeHeader(lineStr, commentPrefix, commentSuffix)
{
	var cIndex = 0;
	while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
	
	if (lineStr.substring(cIndex).startsWith(commentPrefix)) { cIndex += commentPrefix.length; }
	while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
	
	if (cIndex >= lineStr.length || !isHeaderOutlineCharacter(lineStr[cIndex])) { return false; }
	cIndex++;
	if (cIndex < lineStr.length && isWhitespace(lineStr[cIndex]))
	{
		while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
		var endIndex = lineStr.length;
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		if (commentSuffix != "" && lineStr.substring(0,endIndex).endsWith(commentSuffix)) { endIndex -= commentSuffix.length; }
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		if (endIndex <= cIndex || !isHeaderOutlineCharacter(lineStr[endIndex-1])) { return false; }
		endIndex--;
		if (endIndex <= cIndex || !isWhitespace(lineStr[endIndex-1])) { return false; }
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		var headerText = lineStr.substring(cIndex, endIndex);
		//TODO: Should we do any validation on the headerText?
		return true;
	}
	else
	{
		var numHeaderChars = 0;
		while (cIndex < lineStr.length && isHeaderOutlineCharacter(lineStr[cIndex])) { cIndex++; numHeaderChars++; }
		if (numHeaderChars < 3) { return false; }
		while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
		if (commentSuffix != "" && lineStr.substring(cIndex).startsWith(commentSuffix)) { cIndex += commentSuffix.length; }
		while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
		if (cIndex < lineStr.length) { return false; }
		return true;
	}
}

function HeaderInfo(indentationStr, commentSyntax, startCharIndex, endCharIndex, headerText)
{
    this.indentationStr  = indentationStr;
    this.commentSyntax  = commentSyntax;
    this.startCharIndex = startCharIndex;
    this.endCharIndex   = endCharIndex;
    this.headerText     = headerText;
}

function extractHeaderInfo(lineStr, commentPrefix, commentSuffix)
{
	var result = new HeaderInfo("", "", 0, 0, null);
	var cIndex = 0;
	while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { result.indentationStr += lineStr[cIndex]; cIndex++; } //consume whitespace
	
	result.startCharIndex = cIndex;
	if (lineStr.substring(cIndex).startsWith(commentPrefix)) { result.commentSyntax = commentPrefix; cIndex += commentPrefix.length; }
	while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { result.commentSyntax += lineStr[cIndex]; cIndex++; } //consume whitespace
	
	while (cIndex < lineStr.length && isHeaderOutlineCharacter(lineStr[cIndex])) { cIndex++; } //consume header chars
	result.endCharIndex = cIndex;
	while (cIndex < lineStr.length && isWhitespace(lineStr[cIndex])) { cIndex++; } //consume whitespace
	if (cIndex < lineStr.length)
	{
		// if we still haven't reached the end then that probably means this is the middle line of the header which contains text in the center
		// Let's start walking from the end of the line consuming header syntax and then report anything left as the headerText
		var endIndex = lineStr.length;
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		result.endCharIndex = endIndex;
		if (commentSuffix != "" && lineStr.substring(0,endIndex).endsWith(commentSuffix)) { endIndex -= commentSuffix.length; }
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		while (endIndex > cIndex && isHeaderOutlineCharacter(lineStr[endIndex-1])) { endIndex--; } //consume header chars
		while (endIndex > cIndex && isWhitespace(lineStr[endIndex-1])) { endIndex--; } //consume whitespace
		if (endIndex > cIndex)
		{
			result.headerText = lineStr.substring(cIndex, endIndex);
		}
	}
	
	return result;
}

// +--------------------------------------------------------------------------------------+
// |                                       Commands                                       |
// +--------------------------------------------------------------------------------------+
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('TaylorExt is starting up...');
	
	// +--------------------------------------------------------------+
	// |                        taylorExt.test                        |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerCommand('taylorExt.test', function ()
	{
		console.log("Wooo hooo taylor");
		console.error("Oh no taylor!");
		vscode.window.showInformationMessage('Hello World from taylorExt!!! :D Woo hoo');
		vscode.debug.activeDebugConsole.append("This is a debug console message :O");
	}));
	
	// +--------------------------------------------------------------+
	// |                  taylorExt.printSyntaxInfo                   |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.printSyntaxInfo', function (textEditor, edit, args)
	{
		var languageId = textEditor.document.languageId;
		console.log("languageId: " + languageId);
		console.log("Comment Syntax:\nline: \"" + getCommentLineSyntax(languageId) + "\"\nblock: \"" + getCommentBlockStartSyntax(languageId) + "\" \"" + getCommentBlockEndSyntax(languageId) + "\"");
	}));
	
	// +--------------------------------------------------------------+
	// |                   taylorExt.scrollToCursor                   |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerCommand('taylorExt.scrollToCursor', function ()
	{
		vscode.window.activeTextEditor.revealRange(vscode.window.activeTextEditor.selection, vscode.TextEditorRevealType.InCenter);
	}));
	
	// +--------------------------------------------------------------+
	// |                   taylorExt.convertNumber                    |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.convertNumber', function (textEditor, edit, args)
	{
		var arg_from =     (args && args["from"]     !== undefined) ? String(args["from"]).toLowerCase() : "dec";
		var arg_to =       (args && args["to"]       !== undefined) ? String(args["to"]).toLowerCase() : "hex";
		var arg_splitter = (args && args["splitter"] !== undefined) ? String(args["splitter"]) : "";
		var arg_prefix =   (args && args["prefix"]   !== undefined) ? String(args["prefix"]) : "";
		var arg_joiner =   (args && args["joiner"]   !== undefined) ? String(args["joiner"]) : " ";
		// vscode.window.showInformationMessage("Converting " + arg_from + " to " + arg_to + (arg_prefix.length != 0 ? " ( prefix \"" + arg_prefix + "\")" : ""));
		textEditor.selections.forEach(sel =>
		{
			// vscode.window.showInformationMessage("sel: " + JSON.stringify(sel));
			let selectedText = textEditor.document.getText(sel);
			var numbers = [];
			if (arg_from == "ascii")
			{
				for (var cIndex = 0; cIndex < selectedText.length; cIndex++)
				{
					let charValue = my_parseNumber(selectedText.charAt(cIndex), arg_from);
					if (charValue === null) { vscode.window.showErrorMessage("Invalid \"from\" number type \"" + arg_from + "\""); return; }
					if (isNaN(charValue)) { vscode.window.showErrorMessage("Selection \"" + selectedText.charAt(cIndex) + "\" is not a \"" + arg_from + "\" number"); return; }
					numbers.push(charValue);
				}
			}
			else if (arg_splitter.length == 0)
			{
				let numValue = my_parseNumber(selectedText, arg_from);
				if (numValue === null) { vscode.window.showErrorMessage("Invalid \"from\" number type \"" + arg_from + "\""); return; }
				if (isNaN(numValue)) { vscode.window.showErrorMessage("Selection \"" + selectedText + "\" is not a \"" + arg_from + "\" number"); return; }
				numbers.push(numValue);
			}
			else
			{
				let selectedParts = selectedText.split(arg_splitter);
				selectedParts.forEach(selectedPart =>
				{
					let numValue = my_parseNumber(selectedPart, arg_from);
					if (numValue === null) { vscode.window.showErrorMessage("Invalid \"from\" number type \"" + arg_from + "\""); return; }
					if (isNaN(numValue)) { vscode.window.showErrorMessage("Selection \"" + selectedPart + "\" is not a \"" + arg_from + "\" number"); return; }
					numbers.push(numValue);
				});
			}
			
			if (numbers.length == 0) { vscode.window.showErrorMessage("No numbers found to convert"); return; }
			// if (numbers.length > 1) { vscode.window.showInformationMessage("Joiner: \"" + arg_joiner + "\""); }
			
			//check for valid "to" argument
			let toCheckResult = my_encodeNumber(0, arg_to);
			if (toCheckResult === null) { vscode.window.showErrorMessage("Invalid \"to\" number type \"" + arg_to + "\""); return; }
			
			var resultStr = "";
			numbers.forEach(num =>
			{
				if (resultStr.length > 0) { resultStr += arg_joiner; }
				// vscode.window.showInformationMessage("Encoding " + num + " in \"" + arg_to + "\"");
				resultStr += arg_prefix + my_encodeNumber(num, arg_to);
			});
			// vscode.window.showInformationMessage("Final result: \"" + resultStr + "\"");
			
			edit.replace(sel, resultStr);
		});
	}));
	
	// +--------------------------------------------------------------+
	// |                    taylorExt.googleSearch                    |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.googleSearch', function (textEditor, edit, args)
	{
		var arg_kill = (args && args["kill"]) ? Boolean(args["kill"]) : false;
		let selectedText = getSelectedText(); //TODO: Make the aggregate all selections together with spaces inserted in-between
		if (!selectedText) { vscode.window.showErrorMessage("Nothing is selected for google search"); return; }
		const uriText = encodeURI(selectedText);
		// const googleSearchCfg = vscode.workspace.getConfiguration(CFG_SECTION);
		// const queryTemplate = googleSearchCfg.get(CFG_QUERY);
		// const query = queryTemplate.replace("%SELECTION%", uriText);
		var searchString = selectedText.replace(" ", "+");
		//TODO: More URL escaping!
		let query = "https://www.google.com/search?q=" + searchString;
		vscode.commands.executeCommand("vscode.open", vscode.Uri.parse(query));
		if (arg_kill)
		{
			vscode.window.activeTextEditor.selections.forEach(sel => edit.delete(sel));
		}
	}));
	
	// +--------------------------------------------------------------+
	// |                    taylorExt.generateNums                    |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.generateNums', function (textEditor, edit, args)
	{
		//TODO: Maybe we want to always make numbers generate from first (topmost) selection down towards the bottom of the file.
		//      Right now looping over selections seems to respect the order in which the selections were made
		var arg_format = (args && args["format"]) ? String(args["format"]).toLowerCase() : "";
		if (arg_format.length > 0)
		{
			var testFormat = getFormatGeneratedNum(arg_format, 0);
			if (testFormat === null) { vscode.window.showErrorMessage("Invalid generation format \"" + arg_format + "\""); return; }
			
			for (var sIndex = 0; sIndex < vscode.window.activeTextEditor.selections.length; sIndex++)
			{
				let sel = vscode.window.activeTextEditor.selections[sIndex];
				edit.replace(sel, getFormatGeneratedNum(arg_format, sIndex));
			}
		}
		else
		{
			vscode.window.showInputBox({ignoreFocusOut: true, placeHolder: "format (like 0:2 or A:1) [start]:[increment]", prompt: "Format:"})
				.then(function(formatStr)
			{
				vscode.window.showInformationMessage("Generating numbers based off format \"" + formatStr + "\"");
				
				var testFormat = getFormatGeneratedNum(formatStr, 0);
				if (testFormat === null) { vscode.window.showErrorMessage("Invalid generation format \"" + formatStr + "\""); return; }
				
				vscode.window.activeTextEditor.edit(function(editBuilder)
				{
					for (var sIndex = 0; sIndex < vscode.window.activeTextEditor.selections.length; sIndex++)
					{
						let sel = vscode.window.activeTextEditor.selections[sIndex];
						editBuilder.replace(sel, getFormatGeneratedNum(formatStr, sIndex));
					}
				});
			});
		}
	}));
	
	// +--------------------------------------------------------------+
	// |                    taylorExt.alignCursors                    |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.alignCursors', function (textEditor, edit, args)
	{
		let tabSize = vscode.workspace.getConfiguration().get("editor.tabSize");
		var maxColumn = 0;
		vscode.window.activeTextEditor.selections.forEach(sel =>
		{
			let selLineStr = vscode.window.activeTextEditor.document.lineAt(sel.active.line).text;
			let columnNum = getColumnNumberForIndex(tabSize, selLineStr, sel.active.character);
			if (maxColumn < columnNum) { maxColumn = columnNum; }
		});
		// vscode.window.showInformationMessage("Aligning " + vscode.window.activeTextEditor.selections.length + " selections to column " + maxColumn);
		vscode.window.activeTextEditor.selections.forEach(sel =>
		{
			let selLineStr = vscode.window.activeTextEditor.document.lineAt(sel.active.line).text;
			let columnNum = getColumnNumberForIndex(tabSize, selLineStr, sel.active.character);
			if (columnNum < maxColumn)
			{
				let numSpacesToAdd = maxColumn - columnNum;
				// vscode.window.showInformationMessage("Adding " + numSpacesToAdd + " spaces to line " + sel.active.line);
				edit.insert(sel.active, " ".repeat(numSpacesToAdd));
			}
		});
	}));
	
	// +--------------------------------------------------------------+
	// |                   taylorExt.gotoEmptyLine                    |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.gotoEmptyLine', function (textEditor, edit, args)
	{
		var arg_forward = (args && args["forward"] !== undefined) ? Boolean(args["forward"]) : true;
		var arg_select = (args && args["select"] !== undefined) ? Boolean(args["select"]) : false;
		
		var newSelections = [];
		var revealRangeMin = null;
		var revealRangeMax = null;
		for (var sIndex = 0; sIndex < textEditor.selections.length; sIndex++)
		{
			let sel = textEditor.selections[sIndex];
			var currLineNum = sel.active.line;
			var currLineStr = textEditor.document.lineAt(currLineNum);
			do
			{
				currLineNum += (arg_forward ? 1 : -1);
				if (currLineNum >= textEditor.document.lineCount) { vscode.window.showInformationMessage("Hit end of file"); currLineStr = null; break; }
				if (currLineNum < 0) { vscode.window.showInformationMessage("Hit beginning of file"); currLineStr = null; break; }
				currLineStr = textEditor.document.lineAt(currLineNum);
			} while (!currLineStr.isEmptyOrWhitespace);
			
			if (currLineStr === null) //we reached the beginning/end of the file
			{
				if (currLineNum < 0) { currLineNum = 0; }
				else { currLineNum = textEditor.document.lineCount-1; }
				currLineStr = textEditor.document.lineAt(currLineNum);
			}
			if (arg_select) { newSelections.push(new vscode.Selection(sel.anchor, currLineStr.range.end)); }
			else { newSelections.push(new vscode.Selection(currLineStr.range.end, currLineStr.range.end)); }
			if (revealRangeMin == null || currLineStr.range.end.isBefore(revealRangeMin)) { revealRangeMin = currLineStr.range.end; }
			if (revealRangeMax == null || currLineStr.range.end.isAfter(revealRangeMax))  { revealRangeMax = currLineStr.range.end; }
		}
		// vscode.window.showInformationMessage("Selections: " + JSON.stringify(newSelections));
		textEditor.selections = newSelections;
		if (revealRangeMin !== null) { textEditor.revealRange(new vscode.Range(revealRangeMin, revealRangeMax)); }
	}));
	
	// +--------------------------------------------------------------+
	// |                taylorExt.formatCommentHeader                 |
	// +--------------------------------------------------------------+
	context.subscriptions.push(vscode.commands.registerTextEditorCommand('taylorExt.formatCommentHeader', function (textEditor, edit, args)
	{
		var arg_width       = (args && args["width"]       !== undefined) ? parseInt(args["width"])     : 32;
		var arg_corner_char = (args && args["corner_char"] !== undefined) ? String(args["corner_char"]) : "+";
		var arg_top_char    = (args && args["top_char"]    !== undefined) ? String(args["top_char"])    : "-";
		var arg_bottom_char = (args && args["bottom_char"] !== undefined) ? String(args["bottom_char"]) : "-";
		var arg_side_char   = (args && args["side_char"]   !== undefined) ? String(args["side_char"])   : "|";
		
		var commentPrefix = getCommentLineSyntax(textEditor.document.languageId);
		var commentSuffix = "";
		if (commentPrefix == null) { commentPrefix = getCommentBlockStartSyntax(textEditor.document.languageId); commentSuffix = getCommentBlockEndSyntax(textEditor.document.languageId); }
		if (commentPrefix == null || commentSuffix == null) { commentPrefix = ""; commentSuffix = ""; }
		
		var newSelections = [];
		var performedOperation = false;
		for (var sIndex = 0; sIndex < textEditor.selections.length; sIndex++)
		{
			let sel = textEditor.selections[sIndex];
			var currLineNum = sel.active.line;
			var currLine = textEditor.document.lineAt(currLineNum);
			var currLineStr = currLine.text;
			var currLineIndentation = currLineStr.substring(0, currLine.firstNonWhitespaceCharacterIndex);
			// console.log("Checking selection[" + sIndex + "]: \"" + currLineStr + "\"");
			
			// +========================================+
			// | Try to Deconstruct an Existing Header  |
			// +========================================+
			var deconstructedHeader = false;
			if (doesLineLookLikeHeader(currLineStr, commentPrefix, commentSuffix))
			{
				// TODO: This may actually walk too far if there are two headers back to back.
				//      Maybe we should limit the walkback if we find a header line that contains actual headerText?
				var headerBeginningLineNum = currLineNum;
				if (headerBeginningLineNum > 0 && doesLineLookLikeHeader(textEditor.document.lineAt(headerBeginningLineNum-1).text, commentPrefix, commentSuffix)) { headerBeginningLineNum -= 1; }
				if (headerBeginningLineNum > 0 && doesLineLookLikeHeader(textEditor.document.lineAt(headerBeginningLineNum-1).text, commentPrefix, commentSuffix)) { headerBeginningLineNum -= 1; }
				
				if (headerBeginningLineNum + 3 <= textEditor.document.lineCount)
				{
					var headerLine1 = textEditor.document.lineAt(headerBeginningLineNum+0).text;
					var headerLine2 = textEditor.document.lineAt(headerBeginningLineNum+1).text;
					var headerLine3 = textEditor.document.lineAt(headerBeginningLineNum+2).text;
					if (doesLineLookLikeHeader(headerLine1, commentPrefix, commentSuffix) && doesLineLookLikeHeader(headerLine2, commentPrefix, commentSuffix) && doesLineLookLikeHeader(headerLine3, commentPrefix, commentSuffix))
					{
						var headerInfo1 = extractHeaderInfo(headerLine1, commentPrefix, commentSuffix);
						var headerInfo2 = extractHeaderInfo(headerLine2, commentPrefix, commentSuffix);
						var headerInfo3 = extractHeaderInfo(headerLine3, commentPrefix, commentSuffix);
						
						if (!headerInfo1.headerText && headerInfo2.headerText && !headerInfo3.headerText)
						{
							var indentation = headerInfo1.indentationStr;
							var headerText = headerInfo2.headerText;
							var headerRange = new vscode.Range(new vscode.Position(headerBeginningLineNum, headerInfo1.startCharIndex), new vscode.Position(headerBeginningLineNum+2, headerInfo2.endCharIndex));
							
							edit.replace(headerRange, headerText);
							newSelections.push(new vscode.Selection(headerRange.start, headerRange.end));
							performedOperation = true;
							deconstructedHeader = true;
							//TODO: Maybe we want to go remove selections that were within the modified region so that we
							//      don't get double attempts to replace (this seems to fail altogether if we double replace a region)
						}
					}
				}
			}
			
			// +==============================+
			// |     Create a New Header      |
			// +==============================+
			if (!deconstructedHeader && !sel.isEmpty)
			{
				// console.log("We're going to make the current selection into a header");
				var headerTextStr = textEditor.document.getText(new vscode.Range(sel.start, sel.end));
				var headerWidth = arg_width;
				if (headerWidth < headerTextStr.length+4)
				{
					headerWidth = headerTextStr.length+4;
				}
				var numSpacesOnEachSide = (headerWidth-2) - headerTextStr.length;
				var isUnbalanced = ((numSpacesOnEachSide%2) != 0);
				numSpacesOnEachSide = Math.floor(numSpacesOnEachSide/2);
				var commentPrefixStr = commentPrefix;
				var commentSuffixStr = commentSuffix;
				if (commentPrefixStr != "") { commentPrefixStr = commentPrefixStr + " "; }
				if (commentSuffixStr != "") { commentSuffixStr = " " + commentSuffixStr; }
				var firstLine  = currLineIndentation + commentPrefixStr + arg_corner_char + (arg_top_char.repeat(headerWidth-2)) + arg_corner_char + commentSuffixStr;
				var secondLine = currLineIndentation + commentPrefixStr + arg_side_char + (" ".repeat(numSpacesOnEachSide)) + headerTextStr + (" ".repeat(numSpacesOnEachSide + (isUnbalanced ? 1 : 0))) + arg_side_char + commentSuffixStr;
				var thirdLine  = currLineIndentation + commentPrefixStr + arg_corner_char + (arg_bottom_char.repeat(headerWidth-2)) + arg_corner_char + commentSuffixStr;
				var eolChar = (textEditor.document.eol == vscode.EndOfLine.CRLF) ? "\r\n" : "\n";
				edit.replace(currLine.range, firstLine + eolChar + secondLine + eolChar + thirdLine);
				newSelections.push(sel);
				performedOperation = true;
			}
		}
		if (performedOperation)
		{
			textEditor.selections = newSelections;
		}
		else
		{
			vscode.window.showErrorMessage("No headers found. Nothing selected.");
		}
	}));
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
